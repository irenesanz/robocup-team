#include "player.h"
#include <sys/time.h>

Player::Player(string teamName) : Person()
{ 
    id=0;
    _configpath="";
    this->teamName=teamName;
}


void Player::destroy()//(bye)
{
    sock << "(bye)\n";
}
void Player::move(int x, int y) // (move x y)
{
    shippingbox.str("");
    shippingbox << "(move "<< x <<" "<< y <<")\n";
    sock<<shippingbox.str();
}

void Player::get_status() //with this, we can get both body and see sensors (until now, just the ball detection is implemented)
{
    string buffer;
    buffer.clear();
    //buffer=returnsbox.str();
    sock>> buffer;
    // cout << "Buffer (get status): "<<  buffer<< endl;
    parser.parse(buffer);

}

Player :: ~Player()
{
}

void Player ::setID(unsigned short id)
{
    this-> id =id;
}

unsigned short Player:: getID()
{
    return id;
}

void Player::setConfigpath(const char * path)
{
    this-> _configpath=path;
}
string Player:: getConfigpath(){
    return _configpath;
} 

void Player::yell(string message)//(say Message)
{
    shippingbox.str("");
    shippingbox << "(say "<< message<<")\n";
}

