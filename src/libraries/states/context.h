#ifndef CONTEXT_H
#define CONTEXT_H

#include <iostream>
#include "runtoball.h"
#include "kicktogoal.h"
#include "runwithball.h"
#include "searchingball.h"
#include "initposition.h"
#include "kickin.h"
#include "defend.h"
#include "backposition.h"
#include "cornerkick.h"


using namespace std; 

/**
 * \brief Context creator state
 *
 * This class manages the differents states of the players.
 */

class Context
{
private:
    State *state; /** The basic state of our States Machine.*/

public:
    /**
     * \brief Default constructor.
     */
    Context() { state=new Searchingball; }

    /**
     * \brief Default constructor.
     * \param Parser A parser which contains the player and the config.txt information.
     * \param short Player id.
     * \param string The config.txt file path.
     */
    string select_state(Parser,unsigned short, string);
    bool pos;

};

#endif //CONTEX_H
