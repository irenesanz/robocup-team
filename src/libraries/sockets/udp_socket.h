#ifndef UDP_SOCKET_H
#define UDP_SOCKET_H

#include "socket.h"


/**
 * \brief UDP socket
 *
 * This class provides an UDP_socket
 */
class UDP_socket : public Socket
{

private:

    struct sockaddr_in _sender_addr; /** Sender's adress (struct). */

public: 

	/**
	* \brief Default constructor.
    */
	UDP_socket();

    /**
    * \brief Polymorphic write function.
    *
    * \param data This string contains the message
    */
	void write(string data);

    /**
    * \brief Polymorphic read function. Collect the sender address.
    *
    * \param data This string will store the message
    */
	void read (string & data);

    /**
    * \brief With this function we can set the receiver adress
    */
    void setReceiverAdress();

    /**
    * \brief To link a port with a socket (server)
    */
    void bind();

    /**
    * \brief To connect with the server IP
    */
    void connect(const char * argv);

};

#endif // UDP_SOCKET_H
