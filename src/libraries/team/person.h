#ifndef PERSON_H
#define PERSON_H

#include <string>
#include "../sockets/udp_socket.h"
#include <iostream>

using namespace std;

/**
 * \brief Person creator
 *
 * This class provides a person
 */

class Person
{
protected:
    string teamName; /** Team name*/

    /**
     * \brief Default constructor with the team inicialization.
     */
    Person();

public:

    UDP_socket sock; /** Composition of UDP socket */

};

#endif // PERSON_H
