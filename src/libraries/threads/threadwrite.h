#ifndef THREADWRITE_H
#define THREADWRITE_H

#include "thread.h"


class ThreadWrite:public Thread
{
public:
    ThreadWrite():Thread(){}

private:
    virtual void* run(void* param) const;
};

#endif // THREADWRITE_H
