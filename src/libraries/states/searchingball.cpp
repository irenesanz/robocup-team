#include "searchingball.h"

string Searchingball :: command(Parser parser) 
{
   // output.str("");
    vector <float> aux(2);
    parser.getInitPos(aux,id,path);

    if(!parser.objectball)
    {
        //Si no veo el balón.. parser.ball[1]=0!!

        if ((aux[1]>=0)){
	  if(parser.field=="left")
            output<<turn(-moment);
	  if(parser.field=="right")
            output<<turn(moment);
	}
        else if((aux[1]<0)){

	  if(parser.field=="left")
            output<<turn(moment);
	  if(parser.field=="right")
            output<<turn(-moment);

	}
    }

    else if((parser.ball[1]>-10) && (parser.ball[1]<10))
    {
        //cout << "Quieto y veo la pelota a " << parser.ball[1] << " grados." << endl;
        output<<turn(0);
        //cout << parser.ball[1]<<endl;
    }
    else
    {
	moment+=parser.ball[1]*0.1; 
	if (moment<-15 )
		moment=-15; 
	if (moment>15)
		moment=15; 

        //cout << "Giro y veo la pelota a " << parser.ball[1] << " grados." << endl;
        if(parser.ball[1]>5)
            output<<turn(moment);

        else if(parser.ball[1]<-5)
            output<<turn(-moment);
    }

    return output.str();
}



