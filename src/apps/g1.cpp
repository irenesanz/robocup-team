#include <iostream>
#include "../libraries/team/goalie.h"
#include <unistd.h>


string configpath(char **); 

int main(int argc, char *argv[])

{
    Goalie g1(argv[4]);
    ofstream teamconfig;

    if(argc<5){
	cerr<<"too few arguments. usage: ./g x y id teamname field"<<endl; 
	exit(1); 
	}

    int X, Y; 
    string path=configpath(argv);
    g1.setConfigpath(path.c_str()); 
    g1.setID(atoi(argv[3]));
//cout <<g1.getID(); 
	X = atoi(argv[1]);	
	Y = atoi(argv[2]);

   if(g1.getID()==1 || g1.getID()==0)
	teamconfig.open(path.c_str(), ios :: trunc);

    string player;
    g1.field=argv[5];  
   // cout <<g1.field; 
    player=g1.field+"player";
    teamconfig<<player<<g1.getID()<<" "<<X<<" "<<Y<<endl; 	

    g1.move(X,Y);


     g1.move(X,Y);

    while (1)
    {
       g1.catchBall();
    }

    return 0;
}


string configpath(char *argv[])
{
    string trial= realpath(argv[0], NULL);
    string robocuppath="/Robocup";
    size_t found= trial.find(robocuppath)+robocuppath.size();
    string teamname; 
    teamname=argv[4]; 
    string configpath="/config/"+teamname+"config.txt";
    trial.replace(found, configpath.size(), configpath); 

    return trial;
}

