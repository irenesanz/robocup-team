#ifndef INITPOSITION_H
#define INITPOSITION_H

#include "state.h"

/**
 * \brief Inicialize the initial positions state
 *
 * This class inherits from State, and its function is place all the players in their initial position.
 */
class Initposition : public State
{
public:


    /**
     * \brief This virtual function contains the rules to place the player in his initial position, at the beginning or when a goal is scored.
     */
    virtual string command(Parser);

};

#endif //INITPOSITION_H

