#ifndef RUNTOBALL_H
#define RUNTOBALL_H

#include "state.h"


/**
 * \brief Run to ball state
 *
 * This class inherits from State, and its function is running to the ball.
 */
class Runtoball : public State
{
public:

    /**
     * \brief This virtual function contains the rules to run towards the ball.
     */
    virtual string command(Parser);

};

#endif //RUNTOBALL_H

