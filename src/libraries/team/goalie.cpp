#include "goalie.h"

Goalie::Goalie(string teamName) : Player(teamName)
{
    ss.str("");
    ss<<"(init "<<teamName <<" (goalie))\n";
    sock << ss.str();
    ss.str(string());
    string response;
    sock >> response;
    sock.setReceiverAdress();
}


void Goalie::catchBall()
{
    int maxangle=10;
    int catchable_angle=45/2;
    int turnangle=2;

    get_status();

    if(parser.goal)
    {
        shippingbox.str("");
        shippingbox << "(move -50 0)\n";
        sock<<shippingbox.str();
    }

    if(((parser.goal_kick_l) && (parser.field=="left")) || ((parser.goal_kick_r) && (parser.field=="right")))
    {
        //cout << "goal_kick_ activado" << endl;

        if(parser.ball[0]>1)
            run(50);
        else
        {
            kick(60,parser.flag_c[1]+15);
            parser.goal_kick_l=0;
            parser.goal_kick_r=0;
        }
    }

    if(parser.objectball)
    {
        //cout << "Veo la pelota a " << parser.ball[0] << " metros, y " << parser.ball[1] << " grados." << endl;

        if (parser.ball[1]<catchable_angle && parser.ball[1]>-catchable_angle && parser.ball[0]<=1.5){
            catch_dir(parser.ball[1]);
            parser.goalie_catch_ball=1;
        }

        if (parser.goalie_catch_ball)
        {
            if(((abs((int)parser.goal_l[1]<45))&&(parser.field=="left")) || ((abs((int)parser.goal_r[1]<45))&& (parser.field=="right")))
                kick(80,-45);
            else
                kick(80,parser.flag_c[1]+15);

            parser.goalie_catch_ball=0;
        }


        if (parser.ball[0]<10 && parser.ball[0]>1.5)
        {
            if (parser.ball[1]>8 || parser.ball[1]<-8)
            {
                turn(-3*(-1)^((int)(parser.ball[1])));
                //cout << "girandoo a por pelota!!" << endl;
            }
            else
            {
                //cout << "Voy a por tiii pelotaaa!!" << endl;
                run(50);
            }
        }
        else if (parser.ball[1]>maxangle)
            turn (2*turnangle);
        else if (parser.ball[1]<-maxangle)
            turn (-2*turnangle);

        else if (parser.ball[0]>10)
        {
            if(((parser.flag_p_l_c[0]<10) && (parser.field=="left")) || ((parser.flag_p_r_c[0]<10) && (parser.field=="right")))
                run(-50);

            if(((abs((int)parser.flag_p_l_c[1])>45)&&(parser.flag_c[0]>30)) && ((abs((int)parser.flag_p_r_c[1])<45)&&(parser.flag_c[0]>30)))
                run(-60);
        }
    }


    else if(!parser.objectball)
    {
        if (parser.goal && parser.flag_c[1]>15)// && parser.flag_c[1]>-maxangle)
            turn (3*turnangle);
        if (parser.goal && parser.ball[1]<-15)
            turn (-3*turnangle);
        else
            turn(turnangle*10);

        if(((parser.flag_p_l_c[0]<10) && (parser.field=="left")) || ((parser.flag_p_r_c[0]<10) && (parser.field=="right")))
        {
            cout << "¿Correré hacia atrás? " << endl; //SIIIIIIIIIIIIIIIIII!!!!
            run(-50);
        }
        else if(parser.flag_c[0]<30)
        {
            if((parser.flag_c[1]>5)&&(parser.flag_c[1]<-5))
                run(-60);
            else
                turn(3);
        }
    }
}



void Goalie::run(double power) //(dash power)
{
    shippingbox.str("");
    shippingbox << "(dash "<< power <<")\n";
    sock<<shippingbox.str();

}

void Goalie::kick(int power,int direction) //(kick Power Direction)
{
    shippingbox.str("");
    shippingbox << "(kick "<< power <<" "<< direction<<")\n";
    sock<<shippingbox.str();
}

void Goalie::turn(int moment) //(turn Moment)
{
    shippingbox.str("");
    shippingbox << "(turn "<< moment <<")\n";
    sock<<shippingbox.str();
}

void Goalie::turn_neck(int angle) //(turn_neck Angle)
{
    shippingbox.str("");
    shippingbox << "(turn_neck "<< angle <<")\n";
    shippingbox.str("");
}


void Goalie::catch_dir(int dir)//(catch Direction)
{
    shippingbox.str("");
    shippingbox << "(catch "<< dir <<")\n";
}



