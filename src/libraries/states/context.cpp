#include "context.h"

string Context :: select_state (Parser parser,unsigned short a, string b) 
{  
    vector <float> aux(2); 
    parser.getInitPos(aux,state->id,state->path);

    if (parser.goal || parser.half_time)
        //state=new Initposition();
    {
        if(parser.kick_off)
            state=new Searchingball();
        else
            state=new Initposition();
    }

    else if(((parser.goal_kick_l) && (parser.field=="left")) || ((parser.goal_kick_r) && (parser.field=="right"))) 
            state=new Searchingball();
        

    else if((state->id>1 && state->id<7) && (state->moved==false)){
           if(((!parser.objectball) || (parser.ball[1]>20)) && (!state->posicion))
                 state=new Backposition(); 
           else{   
		 if(!parser.objectball)
	            state=new Searchingball(); 
		 else 			
	            state=new Defend(); 
           }
    }

    else if(((parser.goal_kick_l) && (parser.field=="left")) ||((parser.goal_kick_r) && (parser.field=="right"))){
             if(!parser.objectball)
	        state=new Searchingball(); 
             else{
                 if(parser.ball[0]>1 )  
                    state= new Runtoball();           
                 else if(parser.ball[0]<=1 &&  parser.ball[0]>0)
		        state= new Cornerkick(); 
             }
    }

    else if(!parser.objectball || (parser.ball[0]>40)){
            if(!state->posicion)
               state=new Backposition(); 
            else state=new Searchingball(); 
    }

    else if(parser.objectball){
            if(parser.ball[0] > 30)
            {
               if(!state->posicion) state=new Backposition(); 
               else state=new Searchingball();            
            }
            if(parser.ball[0]>1 )
               state= new Runtoball();
           
            else if (parser.ball[0]<=1 &&  parser.ball[0]>0){
		     if(parser.kick_in)
		        state= new Kickin(); 
		     else if (parser.mygoal[0]<25 && parser.mygoal[0]>0)
			  state=new Defend(); 
                     else if(parser.theirgoal[0]>20)
                          state= new Runwithball();
                     else state=new Kicktogoal();		
            }
            else state=new Searchingball();
    }

    else state=new Searchingball();
   
    state->setParams(a, b);
    state->output.str("");
    return state->command(parser);
    delete state;
}
