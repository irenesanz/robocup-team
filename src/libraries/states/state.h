#ifndef STATE_H
#define STATE_H

#include <string>
#include <iostream>
#include <sstream>
#include "../parser/parser.h"

using namespace std; 


/**
 * \brief States
 *
 * This is the basic class for the states system.
 */
class State 
{
public:

    /**
     * \brief This virtual function will be used by the childrens, and manages what must do each state.
     */
    virtual string command(Parser parser)=0;

    /**
     * \brief This function
     * \param short Player identification
     * \param string Path of the initial configuration file.
     */
    void setParams(unsigned short, string);

    stringstream  output; /** This stringstream is a public buffer wich stores the information that will be sent via socket. */

    unsigned short id;  /** This is the player_id number. */
    string path; /** That's the config.txt addres.*/

    bool moved ; /** Variable that becomes true when a defender has moved*/
/*Modif jaime*/
    bool posicion; /** Nos indica si el juagor se encuentra en su posicion*/


protected:

    /**
     * \brief State default constructor, where we iniciate some params.
     */
    State()
    {
        id=0;
        path="";
        moment=5;
	moved=false;
    }

    stringstream dummy; /** This stringstream is a protected buffer that stores the encoded strings for the server.*/


    int moment; /** Default moment for turn()*/


    /**
     * \brief Run ahead with a determined power.
     * \param power Represents the energy you want to perform this task.
     */
    string run(double power);

    /**
     * \brief Kick the ball with a given power and direction.
     * \param power Represents the energy you want to perform this task.
     * \param direction Kick the ball with this angle or direction.
     */
    string kick(int power,int direction);

    /**
     * \brief Turn the player body with this moment.
     * \param moment Turning moment applied over the player.
     */
    string turn(int moment);

    /**
     * \brief This function is used to initialize the position of the player.
     * \param x X initial position
     * \param y Y initial position
     */
    string move(int x, int y);
};
#endif //STATE_H
