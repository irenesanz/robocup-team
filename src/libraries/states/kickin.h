#ifndef KICKIN_H
#define KICKIN_H

#include "state.h"

/**
 * \brief Run around the ball state
 *
 * This class inherits from State, and its function is to place behind the ball.
 */
class Kickin : public State
{
public:

    /**
     * \brief This virtual function contains the rules to place behind the ball.
     */
    virtual string command(Parser);

};

#endif //KICKIN_H

