#include "tcp_socket.h"
#include <cstring>
#include <sys/socket.h>
#include <unistd.h>



TCP_socket::TCP_socket()
{
    _serv_addr.sin_family = AF_INET;
    _sockfd = socket(AF_INET, SOCK_STREAM, 0);
}


void TCP_socket::write(string data)
{
   int n= ::write(_sockfd,data.c_str(),data.size());
    if (n < 0) 
         cerr<<"ERROR writing to socket"<<endl;
    
}

void TCP_socket::read(string &data)
{

    char buffer[1024];
    bzero(buffer,sizeof(buffer));
    int n= ::read(_sockfd,buffer,sizeof(buffer));
   	if (n<0)
		cerr<<"ERROR reading from socket"<<endl;
    data=string(buffer);
}


