#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <strings.h>
#include <cstring>
#include <fstream>
#include <math.h>
#include <algorithm>

#include "dataconditioning.h"

using namespace std;

/**
 * \brief Parser creator
 *
 * This class provides a Robocup parser
 */

class Parser
{

public:
   // void change (float , float );

    int stamina;    /** Energy level . */ //(stamina Stamina Effort)
    int effort;     /** Effort. */
    int clock;	 /** Clock. */
    float speed [2] /** Speed and angle of running. */;//(speed Speed Angle)
    int head_angle; /** The angle btw the neck and the body of the player. */ //(head_angle Angle)

    vector <int> counters;      /** This vector keeps the number of invoked actions.*///(kick dash turn say turn_neck catch move change_view)
    vector <int> distPlayers;   /** This vector keeps the relative distance of all the players that we see at the moment.*/
    vector <int> dirPlayers;    /** This vector keeps the relative direction of all the players that we see at the moment.*/

    vector <float> mygoal; /**My goal. */
    vector <float> theirgoal; /** Their goal. */

    unsigned short p_id;      /** Player's identity number */
    vector <float> player;    /** Player's relative position and identity (distance, direction, team, id) */
    string team_name;   /** Player's team name **/

    vector <float> ball;          /** Ball relative position from the player. */
    vector <float> line_r;        /** Relative position of the right line football field from the player. */ //línea derecha
    vector <float> line_l;        /** Relative position of the left line football field from the player. */ //línea izquierda

    vector <float> line_b;        /** Line b relative position from the player. */
    vector <float> line_t;        /** Line t relative position from the player. */

    vector <float> flag_c;        /** Flag c relative position from the player. */ //centro
    vector <float> flag_c_t ;     /** Flag c t relative position from the player. *///centro arriba
    vector <float> flag_c_b ;     /** Flag c b relative position from the player. */ //centro abajo

    vector <float> flag_l_t ;       /** Flag l t relative position from the player. */
    vector <float> flag_r_t ;       /** Flag r t relative position from the player. */
    vector <float> flag_l_b ;       /** Flag l b relative position from the player. */
    vector <float> flag_r_b ;       /** Flag r b relative position from the player. */

    vector <float> flag_g_l_t ;   /** Flag g l t relative position from the player. *///portería izq arriba
    vector <float> goal_l;        /** Left goal relative position from the player. *///portería izquierda
    vector <float> flag_g_l_b ;   /** Flag g l b relative position from the player. *///portería izq abajo

    vector <float> flag_g_r_t ;   /** Flag g r t relative position from the player. *///portería dcha arriba
    vector <float> goal_r;        /** Right goal relative position from the player. *///portería derecha
    vector <float> flag_g_r_b ;   /** Flag g r b relative position from the player. *///portería dcha abajo

    vector <float> flag_p_l_c ;   /** Flag p l c relative position from the player. *///portería izq
    vector <float> flag_p_r_c ;   /** Flag p l c relative position from the player. *///portería izq

    vector <float>flag_p_r_t ;   /** Flag p r t relative position from the player. *///esquina arriba area dcha
    vector <float>flag_p_r_b ;   /** Flag p r b relative position from the player. *///esquina abajo area dcha
    vector <float>flag_p_l_t ;   /** Flag p l t relative position from the player. *///esquina arriba area izq
    vector <float>flag_p_l_b ;   /** Flag p r b relative position from the player. *///esquina abajo area izq
    vector <float>flag_t_r_20 ;   /** Flag t r 20 relative position from the player. *///mitad medio campo derecho arriba
    vector <float>flag_b_r_20 ;   /** Flag b r 20 relative position from the player. *///mitad medio campo derecho abajo
    vector <float>flag_t_l_20 ;   /** Flag t l 20 relative position from the player. *///mitad medio campo izq arriba
    vector <float>flag_b_l_20 ;   /** Flag b l 20 relative position from the player. *///mitad medio campo izq abajo




    unsigned int time; /** Time. */


    string field; /** Field where the players are. */


    //Miscellaneous

    bool objectball;    /** Boolean variable to check if we have seen the ball in this cycle. */
    bool goal;          /** Boolean variable which becomes true when a goal is scored. */
    bool foul;          /** Boolean variable which becomes true when a foul is produced. */
    bool drop_ball;     /** Boolean variable which becomes true when the ball is dropped. */
    bool play_on;       /** Boolean variable which becomes true when the play mode changes to play_on. */
    bool kick_off;       /** Boolean variable which becomes true when there is a kick_off. */
    bool free_kick;	/** Boolean variable which becomes true when there is a free_kick. */
    bool free_kick_foul;	/** Boolean variable which becomes true when there is a free_kick_foul. */
    bool corner_kick_l;	/** Boolean variable which becomes true when there is a corner_kick.*/
    bool corner_kick_r;
    bool offside;	/** Boolean variable which becomes true when there is a offside. */
    bool half_time;	/** Boolean variable which becomes true when there is a half_time. */
    bool kick_in;
    bool goal_kick_l;
    bool goal_kick_r;
    bool goalie_catch_ball;
    bool before_kick_off;
    bool kick_off_l;
    bool kick_off_r;

    bool nearertoball;

bool searching;

    //functions


    void get_data(string&, const string &, vector<float> &,  bool boolean =0); 


    /**
     * \brief Parser default constructor
     *
     */
    Parser();

    /**
     * \brief This main function executes all our parser functions.
     */
    void parse (string &data);

    /**
     * \brief Finds the relative position of the other players respect one
     */
    void findplayer(string &str, unsigned short id, string &team_name,vector <float> see_info);

    /**
     * \brief Finds the relative position (in float numbers) of one object respect the player
     */
    void findfloats(string &str, const string name, float see_info[]);

    /**
     * \brief Finds the value (in integer numbers) of a player atribute.
     *
     * \returns The value of this atribute.
     */
    int  findint(string &str, const string name);

    /**
     * \brief Finds two values (in integer numbers) of a player atributes.
     */
    void findint2(string &str, const string name, int values[]);

    void findint2(string &str, const string name,  int &value1,  int &value2); //sobrecarga

    /**
     * \brief Finds a string of a player atributes.
     *
     * \returns Theis string.
     */
    string  findstring(string &str, const string name);

    /**
     * \brief With this funtcion we collect the referee(arbiter/arbitrator) messages.
     */
    void hear(string &str, const string name);

    /**
     * \brief With this funtcion we place all the players in their initial position.
     */
    void getInitPos(vector<float> &,unsigned short, string);

    /**
     * \brief With this funtcion we initialize all the variables at the beginnin of each iteration.
     */
    void init();
};

#endif // PARSER_H
