#ifndef RUNWITHBALL_H
#define RUNWITHBALL_H

#include "state.h"

/**
 * \brief Run with ball state
 *
 * This class inherits from State, and its function is running with the ball.
 */
class Runwithball : public State
{
public:

    /**
     * \brief This virtual function contains the rules for running with the ball.
     */
    virtual string command(Parser);

};

#endif //RUNWITHBALL_H

