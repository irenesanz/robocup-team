#ifndef GOALIE_H
#define GOALIE_H

#include"player.h"

class Goalie:public Player
{
public:

    Goalie(string);

    void jump();
    void catchBall(); // Portero -> coger balón




    void catch_dir(int dir); //(catch Direction)

    /**
     * \brief Run ahead with a determined power.
     * \param power Represents the energy you want to perform this task.
     */

    void run(double power); //(dash power)

    //void pass(); PASAR

    /**
     * \brief Kick the ball with a given power and direction.
     * \param power Represents the energy you want to perform this task.
     * \param direction Kick the ball with this angle or direction.
     */
    void kick(int power,int direction);//(kick Power Direction)

    /**
     * \brief Only the goolkeaper can use this function to stop the ball with the hands .
     */
    void ballHandling();

    /**
     * \brief Turn the player body with this moment.
     * \param moment Turning moment applied over the player.
     */
    void turn(int moment); //(turn Moment)

    /**
     * \brief Turn the player neck with this moment.
     * \param angle Angle that player turns his head
     */
    void turn_neck(int angle);//(turn_neck Angle)
};

#endif // GOALIE_H
