#ifndef PLAYER_H
#define PLAYER_H

#include "person.h"
#include <vector>
#include "../states/context.h"
#include "../threads/threadread.h"
#include "../threads/threadwrite.h"
#include "../threads/thread.h"


/**
 * \brief Player creator
 *
 * This class provides a player
 */

class Player:public Person
{
protected:

    stringstream ss; /** This stringstream will containt the messages btw player and server. */

    unsigned short id; /** Player number identificator */

    int timelast; /** How long is from the last sended message */
    int moment; /** Turn moment. */
    int velocity; /** Player velocity (for run, dash). */
    //timelast is used to create a buffer of data to send to the server

    bool sentcommand; 

    Parser parser; /** Composition of Parser class. */ 


    string _configpath; 


public:

    string field; 
    void setConfigpath(const char *); 
    string getConfigpath(); 

    pthread_mutex_t mutex;
 //   ThreadRead leer;
 //   ThreadWrite escribir;
    stringstream returnsbox,shippingbox;


    /**
     * \brief Default constructor with inicialization.
     */
//    Player(string TeamName/*,  char *argv[]*/);
    Player(string TeamName);

    /**
     * \brief Default destructor.
     */
    ~Player();


    /**
     * \brief Destructor [~Player].
     */
    void destroy(); //(bye)

    /**
     * \brief This function is used to initialize the position of the player.
     * \param x X initial position
     * \param y Y initial position
     */
    void move(int x, int y); 

    /**
     * \brief Read the player status and position from the server
     */
    void get_status(); //(sense_body)



    /**
     * \brief Set the ID identification of the player from the bash file.
     */
    void setID(unsigned short); 

    /**
     * \brief Shout a message to the other players in the field.
     */
    void yell(string message);

    /**
     * \brief Get the ID identification of the player
     */
    unsigned short getID(); 

 



};

#endif // PLAYER_H
