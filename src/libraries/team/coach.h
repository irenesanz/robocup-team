#ifndef COACH_H
#define COACH_H
#include "person.h"

class Coach:public Person 
{
private:

 	stringstream ss;
	int _unum, _player_type;

public:

    	Coach();
    	//void strategy();
    	void see(); //(look) or (eye MODE)
    	void listen(); //(ear MODE) -> on/off
    	void yell(string message); //(say message)
    	//void recover(int player_id);//(recover)(only trainer)
    	//string broadcast(string message);
    	int get_score();//(score)
    	//string posBall();//(check_ball)(only trainer)
    	void change_player_type();//(change_player_type UNUM PLAYER_TYPE)
    	void team_name();//(team_names)
};

#endif // COACH_H
