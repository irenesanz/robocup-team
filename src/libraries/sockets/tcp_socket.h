#ifndef TCP_SOCKET_H
#define TCP_SOCKET_H

#include "socket.h"


/**
 * \brief TCP socket creator
 *
 * This class provides an TCP_socket
 */
class TCP_socket : public Socket
{
public: 

    /**
    * \brief Default constructor.
    * \param portno Port number
     */
    TCP_socket();

    /**
    * \brief Polymorphic write function.
    *
    * \param data This string contains the message
    */
    void write(string data);

    /**
    * \brief Polymorphic read function.
    *
    * \param data This string will store the message
    */
    void read (string &data);



};

#endif // TCP_SOCKET_H
