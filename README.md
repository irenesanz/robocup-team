RoboCup Team
===================================================================

Authors:

 *  [Nieves Cubo ]()
 *  [Irene Sanz Nieto](http://www.irenesanz.com)
 
 Index
-------------------------------------------------------------------
 * 1.Introduction
 * 2.Compile and run
     * 2.1. Dependencies
     * 2.2. Compiling
     * 2.3  Running
     * 2.4. Documentation
     * 2.5. More info

1. Introduction 
------------------------------------------------------------------
This repository holds the code of a RoboCup Team developed for the Computer Systems II subject. It consists on a team that can play with other teams through the [RoboCup Soccer Simulator](http://sourceforge.net/apps/mediawiki/sserver/index.php?title=Main_Page).



2. Compile and run 
-------------------------------------------------------------------

###2.1 Dependencies###
-----------------------------------------------------------------

The code has been written in C++ and it needs the RoboCup Soccer Simulator in order to run correctly. 

The following libraries (flex, bison, boost, qt4, zlib, libaudio, Xt, png, gobject) must be installed. Please open a terminal and insert the following command: 


	$ sudo apt-get install flex libqt4-dev libboost-all-dev zlib1g-dev bison libaudio-dev libXt-dev libpng-dev gobjc++ libxi-dev python-gobject-2-dev libxrender-dev libfreetype6-dev libfontconfig1-dev

	
Download the RCSS server and monitor from here: 

* [RCSS Server](http://sourceforge.net/projects/sserver/files/rcssserver/15.1.0/rcssserver-15.1.0.tar.gz/download)
* [RCSS Monitor](http://sourceforge.net/projects/sserver/files/rcssmonitor/15.1.0/rcssmonitor-15.1.0.tar.gz/download)


Install the Software: 

* RCSS Server: 

	$ tar xzvf rcssserver-15.1.0.tar.gz
	
	$ cd rcssserver-15.1.0
	
	$ ./configure
	
	$ make
	
	$ sudo make install

* RCSS Monitor: 

	$ tar xzvf rcssmonitor-15.1.0.tar.gz

	$ cd rcssmonitor-15.1.0

	$ ./configure

	$ make

	$ sudo make install


###2.2 Compiling###
-----------------------------------------------------------------
This project uses CMAKE for compiling. In order to compile, once the project has been downloaded create a folder named "build". Enter that folder and open a terminal there. Insert the commands: 

	$ cmake .. 
	$ make

###2.3 Running###
-----------------------------------------------------------------
In order to run the project, open a terminal and enter the command: 

	$ rcssserver 
	
Open another terminal and type: 

	$ rcssmonitor

Run the script left-team.sh and/or right-team.sh to place the players in the soccer field. 

To start the match, press ctrl + k. 
f2 
The players run in xterms. To close them all, press Alt + F2 and then type : killall -9 xterm. 

###2.4 Documentation###
-----------------------------------------------------------------
* [Latex documentation](https://bytebucket.org/irenesanz/robocup-team/raw/258d007db235de7ef7ba490219bae463069c3aee/doc/final_doxy_documentation.pdf)
* [Deliverable -in Spanish-](https://bytebucket.org/irenesanz/robocup-team/raw/258d007db235de7ef7ba490219bae463069c3aee/doc/Final_documentacion.pdf)
	
###2.5 More info###
-----------------------------------------------------------------


