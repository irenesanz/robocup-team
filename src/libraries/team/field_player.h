#ifndef FIELD_PLAYER_H
#define FIELD_PLAYER_H

#include "player.h"

class Fieldplayer: public Player
{

   public: 
	
    Fieldplayer(string);	
    /**
     * \brief Choose the state accordingly to the parser inputs
     */
    void handle_state();

   private: 
    Context cont; /** This instance of Contex will select the state depending on the inputs. */


};

#endif //FIELD_PLAYER_H
