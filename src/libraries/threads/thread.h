#ifndef THREAD__H
#define THREAD__H


#include <pthread.h>


class Thread{

public:
    void start(void* param = NULL);

    inline bool isRunning(){return _running;}

    virtual ~Thread(){
        if ( isRunning() ){
            end();
        }
        if (_t_obj!=NULL) delete (Thread_Obj*)_t_obj; //free alocated memory
    }

protected:
    /**
     * @brief default constructor
     * Protected to avoid instatiation
     */
    Thread();


    /**
     * @brief thread_function
     * @param param void *
     * @return
     */
    static void * thread_function(void* param);

    void* join();
    void* end();

    bool _terminate; //!< The Thread must terminate

private:
    struct Thread_Obj{
        Thread* thr;
        void* param;
    };

    /**
     * @brief Function that will be executed on the thread
     * @param param void casted pointer to the param for the run function
     */
    virtual void* run(void* param) const = 0;

    pthread_t _thread_id; //!< Thread ID
    Thread_Obj* _t_obj;
    bool _running;
};


#endif
