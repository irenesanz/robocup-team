#include "coach.h"

Coach::Coach()
{
    const char * server= "localhost" ;
    int portno=6001;

    sock.connect(server);

    if (sock.getServer()==0)
    {
        sock.error("Unknown host");
        exit(1);
    }

    sock.setPortno(portno);

    ss.str("");
    ss<< "(init "<<teamName<<")\n";
    sock << ss.str();
    ss.str(string());
    _unum=0;
    _player_type=0;
}

void Coach::see()
{
}

void Coach::listen()
{
}

void Coach::yell(string message)//(say Message)
{
    ss.str("");
    ss << "(say "<< message<<")\n";
    sock<<ss.str();
    ss.str(string());
}

int get_score()
{
}

void Coach::change_player_type()
{
    ss.str("");
    ss<< "(change_player_type "<<_unum <<" "<<_player_type<<")\n";
    sock<<ss.str();
    ss.str(string());
}

void Coach::team_name()
{
    ss.str("");
    ss<< "(team_names)\n";
    sock<<ss.str();
}
