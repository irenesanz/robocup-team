#ifndef DATA_CONDITIONING_H
#define DATA_CONDITIONING_H
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
using namespace std; 

void removing_parenthesis(string &);
void unite_flag_names(vector<string>&);
vector <string> data_to_vector(string &); 


#endif //DATA_CONDITIONING_H
