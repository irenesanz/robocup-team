FILE(GLOB THREADS_SRCS *.cpp) #Los coge todos de una vez

add_library(threads ${THREADS_SRCS})
target_link_libraries(threads pthread)
