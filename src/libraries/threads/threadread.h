#ifndef THREADREAD_H
#define THREADREAD_H

#include "thread.h"

class ThreadRead:public Thread
{
public:
    ThreadRead():Thread(){}

private:
    virtual void* run(void* param) const;
    
};
#endif // THREADREAD_H
