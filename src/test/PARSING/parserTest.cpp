#include "../gtest-1.6.0/fused-src/gtest/gtest.h"
#include "../../libraries/parser/parser.h"

class ParserTest : public testing::Test
{
public:
    Parser parser;
    string cad1, cad2, cad3, cad4, cad5;

    virtual void SetUp()
    {
        cad1 = "(sense_body 0 (view_mode high normal) (stamina 7985 1) (speed 0) (kick 2) (dash 1) (turn 0) (say 0))";
        cad2 = "(see 0 ((flag c) 41.3 14.2) ((flag c t) 46.5 -30) ((flag r t) 95.6 -14) ((flag r b) 102.5 25) ((flag g r b) 93.7 10) ((goal r) 92.8 6) ((flag g r t) 92.8 1) ((flag p r b) 81.5 21) ((flag p r c) 76.7 7) ((flag p r t) 76.7 -7) ((ball) 40.4 14) ((line r) 92.8 90) ((player Jins 1) 5 -18 -0.1 0.6))";
        cad3= "(see 0 ((Player) 6 -19)";
        cad4= "(see 0 ((player) 7 -20)";
        cad5= "(hear 131 referee goal_l_1) ";
    }

    virtual void TearDown()
    {
    }
};

TEST_F(ParserTest,testSenseBody)
{
    parser.parse(cad1);

/*    EXPECT_EQ(2, parser.counters[0]); //kick
    EXPECT_EQ(1, parser.counters[1]); //dash
    EXPECT_EQ(0, parser.counters[2]); //turn
    EXPECT_EQ(0, parser.counters[3]); //say
    EXPECT_EQ(0, parser.counters[4]); //turn_neck
    EXPECT_EQ(0, parser.counters[5]); //catch
    EXPECT_EQ(0, parser.counters[6]); //move
    EXPECT_EQ(1, parser.effort); //Effort
    EXPECT_EQ(7985, parser.stamina); //Stamina
*/
}

TEST_F(ParserTest,testSee)
{
    parser.parse(cad2);

    EXPECT_EQ((float)41.3, parser.flag_c[0]);
    EXPECT_EQ((float)14.2, parser.flag_c[1]);

    EXPECT_EQ((float)46.5, parser.flag_c_t[0]);
    EXPECT_EQ((float)-30, parser.flag_c_t[1]);

    EXPECT_EQ((float)95.6, parser.flag_r_t[0]);
    EXPECT_EQ((float)-14, parser.flag_r_t[1]);

    EXPECT_EQ((float)102.5, parser.flag_r_b[0]);
    EXPECT_EQ((float)25, parser.flag_r_b[1]);

    EXPECT_EQ((float)40.4, parser.ball[0]);
    EXPECT_EQ((float)14, parser.ball[1]);

    EXPECT_EQ((float)92.8, parser.flag_g_r_t[0]);
    EXPECT_EQ((float)1, parser.flag_g_r_t[1]);
    EXPECT_EQ((float)92.8, parser.line_r[0]);
    EXPECT_EQ((float)90, parser.line_r[1]);
/*
    EXPECT_EQ(1,parser.p_id);
    EXPECT_EQ("Jins", parser.team_name);
    EXPECT_EQ(5, parser.player[0]);
    EXPECT_EQ(-18, parser.player[1]);
*/

}

TEST_F(ParserTest,testfindplayer2)
{
    parser.parse(cad3);
/*
    EXPECT_EQ(6, parser.player[0]);
    EXPECT_EQ(-19, parser.player[1]);
*/
}


TEST_F(ParserTest,testfindPlayer3)
{
    parser.parse(cad4);

 //   EXPECT_EQ(7, parser.player[0]);
 //   EXPECT_EQ(-20, parser.player[1]);

}

TEST_F(ParserTest,testhearGoal)
{
    parser.parse(cad5);

    EXPECT_EQ(true, parser.goal);
    EXPECT_EQ(false, parser.play_on);
    EXPECT_EQ(false, parser.foul);
    EXPECT_EQ(false, parser.drop_ball);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
