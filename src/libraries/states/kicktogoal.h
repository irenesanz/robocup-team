#ifndef KICKTOGOAL_H
#define KICKTOGOAL_H

#include "state.h"

/**
 * \brief Kick the ball to the porter's lodge state
 *
 * This class inherits from State, and its function is to kick the ball to the porter's lodge.
 */
class Kicktogoal : public State
{
public:

    /**
     * \brief This virtual function contains the rules to goal.
     */
    virtual string command(Parser parser);

};
#endif //KICKTOGOAL_H
