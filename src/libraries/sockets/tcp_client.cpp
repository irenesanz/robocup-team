#include "tcp_client.h"
//#include <stdio.h>

TCP_client::TCP_client(char *name, int portno)
{
    if (_clientSocket.getSockfd() < 0)
        _clientSocket.error("opening socket");
    _server = _clientSocket.getServer();
    _server = gethostbyname(name);
    if (_server == 0) _clientSocket.error("Unknown host");
    _serv_addr = _clientSocket.getServ_addr();
    //	bzero((char *) &_serv_addr, sizeof(_serv_addr));
    bcopy((char *)_server->h_addr, (char *)&_serv_addr->sin_addr.s_addr, _server->h_length);
    _clientSocket.setPortno(portno);
    //	_clientSocket.setServer(_server);
    if (connect(_clientSocket.getSockfd(),(struct sockaddr *)_serv_addr, sizeof(*_serv_addr)) != 0)
        _clientSocket.error("connecting");
}

void TCP_client :: write(string data)
{

    _clientSocket.write(data);

}

void TCP_client::read (string &data)
{

    _clientSocket.read(data);

}

TCP_client & TCP_client:: operator << (string data)
{
   // fgets(_clientSocket.buffer,255,stdin);
    this->write(data);
    return *this;
}

TCP_client & TCP_client::operator >> (string & data)
{	
    this->read(data);
    return *this;
}

