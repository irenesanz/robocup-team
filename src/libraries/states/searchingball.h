#ifndef SEARCHINGBALL_H
#define SEARCHINGBALL_H

#include "state.h"

/**
 * \brief Searching ball state
 *
 * This class inherits from State, and its function is to find the ball.
 */
class Searchingball : public State
{
public:

    /**
     * \brief This virtual function is use to turn, until the player find the ball.
     */
    virtual string command(Parser);

};

#endif //SEARCHINGBALL_H

