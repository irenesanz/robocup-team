#include "state.h"


string State::run(double power) 
{
    dummy.str("");
    dummy << "(dash "<< power <<")\n";
    return dummy.str();
}

string State::kick(int power,int direction) //(kick Power Direction)
{
    dummy.str("");
    dummy << "(kick "<< power <<" "<< direction<<")\n";
    return dummy.str();
}


string State::turn(int moment) //(turn Moment)
{  
    dummy.str("");
    dummy << "(turn "<< moment <<")\n";
    return dummy.str();
}

string State::move(int x, int y) // (move x y)
{
    dummy.str("");
    dummy << "(move "<< x <<" "<< y <<")\n";
    return dummy.str();
}

void State:: setParams(unsigned short ident, string path)
{
    this->id=ident;
    this-> path=path;

}
