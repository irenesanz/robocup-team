#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include "tcp_socket.h"

/**
 * \brief TCP client creator
 *
 * This class provides a TCP_client
 */
class TCP_client
{

private:

    TCP_socket _clientSocket; /** Socket TCP . */
    struct hostent *_server; /** Server pointer (Hostent struct). */
    struct sockaddr_in *_serv_addr; /** Server addres (Sockaddr_in struct) */

public:

    /**
    * \brief Default constructor.
    * \param name IP adress.
    * \param portno Port number.
     */
    TCP_client(char *name, int portno);

    /**
    * \brief Function that invoques the polymorphic write function.
    *
    * \param data This string contains the message.
    */
    void write(string data);

    /**
    * \brief Function that invoques the polymorphic read function.
    *
    * \param data The message will be stored in this string adress.
    */
    void read (string &data);

    /**
     * \brief Overload the operator >>, which calls the function write of this class
     *
     * \param data Information sended
     */
    TCP_client & operator << (string data);

    /**
     * \brief Overload the operator >>, which calls the function read of this class
     *
     * \param data Information received
     */
    TCP_client & operator >> (string & data);
};

#endif // TCP_CLIENT_H
