#include "socket.h"
#include <sstream>

Socket :: Socket ()
{
    _sockfd=0;
    _portno=0;
    _serv_addr.sin_addr.s_addr= INADDR_ANY;
    _serv_addr.sin_family=AF_INET;
    _serv_addr.sin_port=0;
    _server=NULL;

}


int Socket::getSockfd()
{
    return _sockfd;
}

int Socket::getPortno()
{
    return _portno;
}

struct hostent *Socket::getServer()
{
    return _server;
}

struct sockaddr_in *Socket ::getServ_addr()
{
    return &_serv_addr;
}

void Socket::setSockfd(int sockfd)
{
    _sockfd=sockfd;
}

void Socket::setPortno(int portno)
{

    _portno=portno;
    _serv_addr.sin_port = htons(_portno);
}

void Socket::setServer(struct hostent *server)
{
    _server=server;
    _serv_addr.sin_addr.s_addr = *((unsigned long *) _server->h_addr_list[0]);
}

void Socket::error(string err)
{
    cerr<<"error on " <<err<<endl;
}

Socket & Socket:: operator << (string data)
{
    this->write(data);
    return *this;
}


Socket & Socket::operator >> (string & data)
{	
    this->read(data);
    return *this;
}


void Socket::printAll()
{
    cout << "---> Socket <--- " << endl;
    cout << "Sockfd: " << _sockfd << endl;
    cout << "Portno: " << _portno << endl;
    cout << "---> Server_addr <--- " << endl;
    cout << "server sin family: " << _serv_addr.sin_family << endl;
    cout << "server sin addr: " << _serv_addr.sin_addr.s_addr << endl;
    cout << "server sin port: " << _serv_addr.sin_port<< endl;
    cout << "---> Server <--- " << endl;
    cout << "server h_addr list " << &_server->h_addr_list<< endl;

}
