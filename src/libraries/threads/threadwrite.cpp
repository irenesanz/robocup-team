#include "threadwrite.h"
#include <iostream>
#include <cstring>
#include <string.h>
#include <sys/time.h>
#include "../team/player.h"
#include <unistd.h>

using namespace std;

void* ThreadWrite::run(void *param) const
{
    Player* p1 = (Player*) param;

    while(1){

            pthread_mutex_lock(&p1->mutex);
	   //	cout<<p1->shippingbox.str(); 

            p1->sock<<p1->shippingbox.str();
	    p1->shippingbox.str(string());
            pthread_mutex_unlock(&p1->mutex);

            usleep(200);


    }
	   delete p1; 
           return 0;

}
