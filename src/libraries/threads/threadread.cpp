#include "threadread.h"
#include <iostream>
#include <cstring>
#include <string.h>
#include "../team/player.h"
#include <sys/time.h>
#include <unistd.h>

using namespace std;

void* ThreadRead::run(void *param) const
{
     Player* p1 = (Player*) param;
     string aux;

     while(1)
     {
            pthread_mutex_lock(&p1->mutex);

            p1->returnsbox.str(string());
            p1->sock>>aux;
            p1->returnsbox.str(aux);
            pthread_mutex_unlock(&p1->mutex);

            usleep(200);


     }
	delete p1; 
        return 0;

}
