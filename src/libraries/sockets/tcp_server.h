#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <vector>
#include "tcp_socket.h"


/**
 * \brief TCP server creator
 *
 * This class provides a TCP_server
 */
class TCP_server 
{

private:

    TCP_socket _serverSocket; /** Socket TCP - Server. */
    TCP_socket _newclient; /** Socket TCP - New client. **/
    vector <int> _new_client; /** Vector of clients queue . */

    struct hostent *_server; /** Server pointer (Struct). */
    struct sockaddr_in cli_addr; /** Client's adress (Struct). */
    unsigned int clilen; /** Unsigned integer that stores the cli_addr (Struct) size. */
    int i; /** New client vector iterator **/


public: 

    /**
    * \brief Default constructor.
    * \param portno Port number and maxclients maximum number of clients the server can handle at a time
     */
    TCP_server(int, int);

    /**
     * \brief Accept the connection
     *
     * \return variable that is 1 if there was an error on accept or on fork
     */
    int accept();

    /**
    * \brief write function.
    *
    * \param data This string contains the message
    */
    void write(string data);

    /**
    * \brief read function.
    *
    * \param data This string will store the message
    */
    void read (string &data);

    /**
     * \brief Overload the operator >>, which calls the function write of this class
     *
     * \param data Information sended
     */
    TCP_server & operator << (string data);

    /**
     * \brief Overload the operator >>, which calls the function read of this class
     *
     * \param data Information received
     */
    TCP_server & operator >> (string & data);
};

#endif // TCP_SERVER_H
