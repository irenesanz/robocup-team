#ifndef DEFEND_H
#define DEFEND_H

#include "state.h"

/**
 * \brief Searching ball state
 *
 * This class inherits from State, and its function is to find the ball.
 */
class Defend : public State
{
public:

    /**
     * \brief This virtual function is use to turn, until the player find the ball.
     */
    virtual string command(Parser);

};

#endif //DEFEND_H

