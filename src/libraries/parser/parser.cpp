#include "parser.h"

Parser::Parser()
{
    Parser::init();

    objectball=false;
    goal=false;
    play_on=false;
    drop_ball=false;
    foul=false;
    nearertoball=true;

    goal_kick_l=false;
    goal_kick_r=false;
    kick_off=false;
    free_kick=false;
    free_kick_foul=false;
    before_kick_off=false;
    corner_kick_l=false;
    corner_kick_r=false;
    offside=false;
    half_time=false;
    kick_in=false;
    goalie_catch_ball=false;
    kick_off_l=false;
    kick_off_r=false;

    searching=false;
    init(); 
}

void Parser::init(){

        ball.clear();

        line_r.clear();
        line_l.clear();

        line_b.clear();
        line_t.clear();


        flag_c.clear();
        flag_c_t .clear();
        flag_c_b .clear();

        flag_r_t .clear();
        flag_r_b .clear();

        flag_g_l_t.clear();
        goal_r.clear();
        flag_g_l_b.clear();

        flag_g_r_t.clear();
        goal_l.clear();
        flag_g_r_b.clear();

        flag_p_l_c.clear();
        flag_p_r_c.clear();


        mygoal.clear();
        theirgoal.clear();

        player.clear();



   	distPlayers.clear();

	counters.resize(7); 
        ball.resize(2);

        line_r.resize(2);
        line_l.resize(2);

        line_b.resize(2);
        line_t.resize(2);

        ////cout<<"Linea l: Angulo: " << line_l[1] <<endl;

        flag_c.resize(2);
        flag_c_t .resize(2);
        flag_c_b .resize(2);
        //cout<<"Linea t: Angulo: " << line_t[1] <<endl;
        //cout<<"********************************"<<endl;

        flag_r_t .resize(2);
        flag_r_b .resize(2);

        flag_g_l_t.resize(2);
        goal_r.resize(2);
        flag_g_l_b.resize(2);

        flag_g_r_t.resize(2);
        goal_l.resize(2);
        flag_g_r_b.resize(2);

        flag_p_l_c.resize(2);
        flag_p_r_c.resize(2);

 	flag_p_r_t.resize(2);  
        flag_p_r_b.resize(2);    
        flag_p_l_t.resize(2);    
        flag_p_l_b.resize(2);


        mygoal.resize(2);
        theirgoal.resize(2);

        player.resize(2);


    	team_name.resize(1);
    	objectball=true;


}


void Parser:: parse (string &data)
{

  	hear(data, "(hear ");

	
        get_data(data, "ball", ball, objectball);  


	get_data(data, "liner", line_r); 
	get_data(data, "linel", line_l); 

	get_data(data, "lineb", line_b); 
	get_data(data, "linet", line_t); 

	get_data(data, "goalr", goal_r); 
	get_data(data, "goall", goal_l); 


        get_data(data, "flagc", flag_c);

        get_data(data, "flagct", flag_c_t);
        get_data(data, "flagcb", flag_c_b);

        get_data(data, "flagrt", flag_r_t);
        get_data(data, "flagrb", flag_r_b);

        get_data(data, "flagglt", flag_g_l_t);
        get_data(data, "flagglb", flag_g_l_b);
        get_data(data, "flaggrt", flag_g_r_t);

        get_data(data, "flagplc", flag_p_l_c);
        get_data(data, "flagprc", flag_p_r_c);

        get_data(data, "flagplt", flag_p_l_t);
        get_data(data, "flagprt", flag_p_r_t);
        get_data(data, "flagprb", flag_p_r_b);

        get_data(data, "flagplb", flag_p_l_b);

        get_data(data, "flagtr20", flag_t_r_20);
        get_data(data, "flagbr20", flag_b_r_20);

        get_data(data, "flagtl20", flag_t_l_20);        
        get_data(data, "flagbl20", flag_b_l_20);




        findplayer(data, p_id, team_name, player);

   //     head_angle=findint(data, "head_angle");

   

/*    if(field=="right"){
        for (int i=0; i<6; i++){
            mygoal[i]=goal_r[i];
            theirgoal[i]=goal_l[i];
        }
    }
    if(field=="left"){
        for (int i=0; i<6; i++){
            mygoal[i]=goal_l[i];
            theirgoal[i]=goal_r[i];
        }
    }
*/
 


    //    cout << data << endl;
    //    cout << "Time: "<<time<<endl;
    //    cout << "Arg: " << ball[0] << "  Ang: "<<ball[1] << endl;
    //    cout << "Speed arg: " << speed[0] << "  Speed ang: " << speed[1] << endl;
    //    cout << "Stamina: " << stamina << "  Effort: " << effort << endl;
    //    cout << "Line r: " << line_r[0] << " Line l: " << line_l[0] << endl;
    //    cout << "Flag c: " << flag_c[0] <<  "  Flag c t: " << flag_c_t[0] << " Flag c b: " << flag_c_b[0] << endl;
    //    cout << "Flag g l t: " << flag_g_l_t[0] << " Flag g l b: " << flag_g_l_b[0] << endl;
    //    cout << "Flag g r t: " << flag_g_r_t[0] << " Flag g r b: " << flag_g_r_b[0] << endl;
    //    cout << "Goal r: " << goal_r[0]<<" " <<goal_r[1] << " Goal l: " << goal_l[0]<<" "<<goal_l[1]<< endl;
    //    cout << "Mygoal: " << mygoal[0]<<" " <<mygoal[1] << " Theirgoal: " << theirgoal[0]<<" "<<theirgoal[1]<< endl;
    //    cout << "Pid: " <<p_id[0] <<  "   Team:  " <<team_name << "  PLayer pos: " << player[0] << "  "<< player[1] << endl;




}


void Parser::get_data(string &data, const string &name, vector<float>& result, bool boolean)
{
	vector <string>dummy; 
	dummy=data_to_vector(data); 
	int counter=0; 
	for (int i=0; i<dummy.size(); i++){
		if (dummy[i]==name)
		{
		    for (int j=0; j<result.size();j++) {
		      if(i+j+1 <= dummy.size())
			result[j]=(atof(dummy[i+j+1].c_str())); 
			}
		boolean=true; 
		}

	}
}


void Parser:: findplayer(string &str, unsigned short id, string &team_name,vector <float> see_info)
{
    int a=-1,b=-1,c=-1;
    size_t found=0, found2=0, found3=0, foundy=0;
    distPlayers.clear();
    dirPlayers.clear();

    //Probamos las distintas configuraciones
    string name="(player ";
    string name2="(player) "; //de mi equipo
    string name3= "(Player) "; //del otro equipo


    while (found!=string::npos)
    {
        found=str.find(name,found+1);
        a++;
    }
    found=str.find(name,0);

    while (found2!=string::npos)
    {
        found2=str.find(name2,found2+1);
        b++;
    }
    found2=str.find(name2,0);

    while (found3!=string::npos)
    {
        found3=str.find(name3,found3+1);
        c++;
    }
    found3=str.find(name3,0);

    //cout << "abc: " << a << " " << b << " " << c << endl;

    for (int w=0; w<(a+b+c); w++)
    {
        bool our_player=false;

        size_t id_t=0, n1=0, n2=0,n3=0,n4=0, final=0;
        int j=0, k=0, l=0, m=0;
        string id_number, team, Distance, Direction, DistChange, DirChange;// BodyFacingDir, HeadFacingDir;

        if (found!= string::npos)
        {
            id_number.clear();
            Direction.clear();
            Distance.clear();
            DistChange.clear();
            DirChange.clear();
            team.clear();

            found+=name.length();
            id_t=str.find(" ", found);
            n1=str.find(") ", id_t);
            n2=str.find(" ", n1+2);
            n3=str.find(" ", n2+1);
            n4=str.find(" ", n3+1);
            final=str.find(")", n4+1);

            // cout << "posiciones: Found:" << found << " id: " << id_t << " n1:  " << n1 << " n2:  "<< n2 <<" n3:  "<<n3 << " n4:  " <<n4 << " final:  " << final<< endl;

            for (size_t i= found; i <= final; i++)
            {
                //team name
                if ((found<=i)&&(i<id_t))
                {
                    team[j]=str.at(i);
                    j++;
                }
                else if (i==id_t)
                {
                    team[j]= '\0';
                    team_name= team.c_str();
                }

                //id number
                else if ((id_t<i)&&(i<n1))
                {
                    id_number[k]=str.at(i);
                    k++;
                }

                else if (i==n1)
                {
                    id_number[k]='\0';
                    id= atof(id_number.c_str());
                }

                //Distance
                else  if ((n1<i)&&(i<n2))
                {
                    Distance[l]=str.at(i);
                    l++;
                }

                else if (i==n2)
                {
                    Distance[l]= '\0';
                    see_info[0]= atoi(Distance.c_str());
                }

                //Direction
                else if ((n2<i)&&(i<n3))
                {
                    Direction[m]=str.at(i);
                    m++;
                }

                else if (i==n3)
                {
                    Direction[m]= '\0';
                    see_info[1]= atoi(Direction.c_str());
                }
            }distPlayers.push_back(see_info[0]);
            dirPlayers.push_back(see_info[1]);
        }

        else if ((found2!= string::npos)||(found3!= string::npos))
        {
            if (b>0)//found2
            {
                our_player=true;
                found2+=name2.length();// name2 = name3 en longitud
                n1=str.find(" ", found2);
                n2=str.find(")", n1);
                foundy=found2;
            }

            else if (c>0)//found3
            {
                found3+=name3.length();
                n1=str.find(" ", found3);
                n2=str.find(")", n1);
                foundy=found3;
            }

            Direction.clear();
            Distance.clear();



            for (size_t i= foundy; i <= n2; i++)
            {
                //Distance
                if ((foundy<=i)&&(i<n1))
                {
                    Distance[l]=str.at(i);
                    l++;
                }

                if (i==n1)
                {
                    Distance[l]= '\0';
                    see_info[0]= atoi(Distance.c_str());
                }

                //Direction
                if ((n1<i)&&(i<n2))
                {
                    Direction[m]=str.at(i);
                    m++;
                }

                if (i==n2)
                {
                    Direction[m]= '\0';
                    see_info[1]= atoi(Direction.c_str());
                }
            }distPlayers.push_back(see_info[0]);
            dirPlayers.push_back(see_info[1]);
        }

        //        if (our_player==true)
        //            cout << "\n Visto jugador (" << w+1 << " de " << a+b+c << ") a " << see_info[0] << " metros."<< endl;
        //        else
        //            cout << "\n Visto jugador (del otro equipo) (" << w+1 << " de " << a+b+c << ") a " << see_info[0] << " metros."<< endl;

    }

    //    if(distPlayers.size()>=1)
    //    {
    //        cout << "Jugadores a: " << endl;

    //        for(int t=0; t<distPlayers.size(); t++)
    //        {
    //            cout << distPlayers.at(t)<<"m ";
    //        }
    //    }

}



void Parser:: hear(string &str, const string name)
{
    int found_name, found_drop, found_play_on, found_goal, found_foul, found_kick_off, found_free_kick, found_goal_kick_l, found_goal_kick_r;
    int found_corner_kick_l, found_corner_kick_r, found_offside, found_goalie_catch, found_before_kick_off, found_free_kick_foul;
    int found_half_time, found_kick_in, found_kick_off_l, found_kick_off_r;

    found_name=str.find(name);

    if(found_name!=string::npos)
    {
        cout<< "\n Oído cocina: " << str << endl;

        found_drop=str.find("drop_ball");
        found_play_on=str.find("play_on");
        found_goal=str.find(" goal_");
        found_foul=str.find(" foul_");
        found_kick_off=str.find(" kick_off_");
        found_free_kick=str.find(" free_kick_");
        found_corner_kick_l=str.find(" corner_kick_l");
        found_corner_kick_r=str.find(" corner_kick_r");
        found_offside=str.find(" offside_");
        found_half_time=str.find(" half_time");
        found_kick_in=str.find(" kick_in_");
        found_goalie_catch=str.find(" goalie_catch_ball_");
        found_free_kick_foul=str.find(" free_kick_foul_");
        found_before_kick_off=str.find(" before_kick_off");
        found_goal_kick_l=str.find("referee goal_kick_l");
        found_goal_kick_r=str.find("referee goal_kick_r");
        found_kick_off_l=str.find(" kick_off_l");
        found_kick_off_r=str.find(" kick_off_r");

        if(found_drop!= string::npos)
        {
            cout << "DROP_BALL: La pelota ha cambiado de posición. Búscala!"<< endl;
            drop_ball=1;
    	    init(); 

        }

        else if(found_goal_kick_l!= string::npos)
        {
            cout << "GOAL KICK IZQUIERDA: Portero saca " << endl;
            //goal_kick_l=1;
            found_kick_in=1;
            init();
        }

        else if(found_goal_kick_r!= string::npos)
        {
            cout << "GOAL KICK DERECHA: Portero saca " << endl;
            //goal_kick_r=1;
            found_kick_in=1;
            init();
        }
        else if(found_play_on!= string::npos)
        {
            cout << "PLAY_ON: Se reanuda el juego."<< endl;
            play_on=1;

            goal=0;
            drop_ball=0;
            goalie_catch_ball=0;

            before_kick_off=0;
            kick_off=0;
            kick_off_r=0;
            kick_off_l=0;
            half_time=0;
            kick_in=0;

    	    init(); 

        }

        else if(found_goal!= string::npos)
        {
            cout << "GOAL: Han marcado gol. Volver a posición inicial"<< endl;
            goal=1;

        }

        else if(found_foul!= string::npos)
        {
            cout << "FOUL: falta!!! ¿Free kick?" << endl;
            foul=1;
    	    init(); 
        }

        else if(found_free_kick != string::npos)
        {
            cout << "FREE_KICK: Tiro libre (sacar una falta) " << endl;
            kick_in=1;
    	    init(); 

        }

        else if(found_corner_kick_l != string::npos)
        {
            cout << "CORNER_KICK IZQUIERDA: Corner" << endl;
            //kick_in=1;
            corner_kick_l=1;
    	    init(); 

        }
        else if(found_corner_kick_r != string::npos)
        {
            cout << "CORNER_KICK DERECHA: Corner" << endl;
            //kick_in=1;
            corner_kick_r=1;
    	    init(); 

        }
        else if(found_offside != string::npos)
        {
            cout << "OFFSIDE: Fuera de juego" << endl;
            offside=1;
    	    init(); 

        }

        else if(found_half_time != string::npos)
        {
            cout << "HALF TIME: Descanso" << endl;
            half_time=1;

        }

        else if(found_kick_in != string::npos)
        {
            cout << "KICK IN: Saque de banda" << endl;
            kick_in=1;
            before_kick_off=0;
    	    init(); 

        }

        else if(found_goalie_catch!= string::npos)
        {
            cout << "PORTERO COGE PELOTA: lanzarla lejos"<< endl;
            goalie_catch_ball=1;

        }

        else if(found_free_kick_foul != string::npos)
        {
            cout << "FREE_KICK_FOUL: Tiro libre (sacar una falta) " << endl;
            kick_in=1;
    	    init(); 

        }

        else if(found_before_kick_off!= string::npos)
        {
            cout << "BEFORE KICK OFF: PREPARADOS " << endl;
            before_kick_off=1;
    	    init(); 

        }

        
        else if(found_kick_off_l!= string::npos)
        {
            cout << "KICK OFF LEFT: Saca equipo de la izq " << endl;
            kick_off_l=1;
    	    init(); 

        }
        else if(found_kick_off_r!= string::npos)
        {
            cout << "KICK OFF RIGHT: Saca equipo de la derecha " << endl;
            kick_off_r=1;
            init();
        }
        else if(found_kick_off != string::npos)
        {
            cout << "KICK_OFF: Inicio " << endl;
            kick_in=1;
            kick_off=1;
            goal_kick_l=0;
            goal_kick_r=0;
            before_kick_off=0;
            init();
        }

    }
}

void  Parser:: findint2(string &str, const string name,  int &value1,  int &value2)
{
    size_t found=0;
    int j=0, n=0;
    string solution=" ";

    found=str.find(name);

    if (found!= string::npos)
    {
        for (size_t i= found+name.length(); i < str.length(); i++)
        {
            if (str.at(i) == ' ')
                break;
            solution[j]=str.at(i);
            j++;
            n=(int)i;
            if (i==str.length()-1){
                solution[j]='\0';
            }
        }

        value1= atoi(solution.c_str());
        j=0;
        n=n+2;

        if (str.at(n)!= ')')
        {
            solution=" ";

            for (size_t k= n; k < str.length(); k++)
            {
                if (str.at(k) == ')')
                    break;
                solution[j]=str.at(k);
                j++;
                if (k==str.length()-1)
                    solution[j]='\0';

            }
            value2= atoi(solution.c_str());
        }
    }
}



/******************************* FILE PARSER *************************************/
void Parser:: getInitPos(vector<float> &ret, unsigned short id, string path)
{
    ret.clear();
    ifstream configfile(path.c_str());
    int x,y;
    stringstream coord;
    coord.str("");

    coord<<"player"<<id<<" ";
    string dummy;

    while(getline (configfile, dummy))
    {
        dummy+="\0";
        Parser::findint2(dummy, coord.str(), x,y);
    }

    vector<float>returning;
    returning.push_back(x);
    returning.push_back(y);
    ret=returning;
    //cout <<coord.str()<<" "<<ret[0]<<" " <<ret[1]<<endl;
}


