#include "dataconditioning.h"

void removing_parenthesis(string &str)
{
   size_t  found=0, lastfound=0, limitfound=0;

//This part checks if there are any variables without spaces in between, with just parenthesis. 
 do{
        found = str.find(")(", lastfound);
        limitfound = str.find(")(", found+1);
	//str.replace(found, 1, " "); //throwing exception because of the white space :S 
	lastfound=found;
   }while (limitfound!=string ::npos);



//The parenthesis are removed so there are only spaces between variables
   str.erase(std::remove(str.begin(), str.end(), '('), str.end());
   str.erase(std::remove(str.begin(), str.end(), ')'), str.end());
}


void unite_flag_names(vector<string>&result)
{

  string dummy; 
  for(unsigned i=0; i<result.size() ; i++){

	if (result[i]=="flag" || result[i]=="line" || result[i]=="goal")
	{
		dummy=result[i]+result[i+1];
		result[i]=dummy; 
		result.erase(result.begin()+i+1); 


		if (result[i+1]=="r"|| result[i+1]=="l" || result[i+1]=="b" ||result[i+1]=="t")
			{
 			dummy+=result[i+1];
			result[i]=dummy; 
			result.erase(result.begin()+i+1);
			if(result[i+1]=="t"||result[i+1]=="b"||result[i+1]=="c"||result[i+1]=="20"){
				dummy+=result[i+1];
				result[i]=dummy; 
				result.erase(result.begin()+i+1);
				}

			}	
		 

	}
}
}

vector <string> data_to_vector(string &str) 
{
   size_t found, lastfound, dummy, forward=0;
   vector <string> result; 
   removing_parenthesis(str); 
   bool onetime=true; 

   char aux[29]; 
	do{
	   found=str.find(" ", forward);
	   lastfound=str.find(" ", found+1);
 	   //cout <<(int)found <<endl<<(int)lastfound<<endl;

	   if(onetime){
	    dummy=str.copy(aux,(int)(found),0); 
	    forward=found; 
	   }
	   else{
	   dummy=str.copy(aux,(int)(lastfound-found-1), (int)found+1); 
	   forward=lastfound; 
	   }
	   aux[dummy]='\0';
	   result.push_back(aux); 
	  // cout <<aux<<endl; 
	    onetime=false;
	}while (lastfound != string :: npos); 

   unite_flag_names(result); 
    
   return result;
}
