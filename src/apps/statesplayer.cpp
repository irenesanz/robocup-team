#include "../libraries/team/field_player.h"
#include <iostream>
#include <fstream>
#include <unistd.h>


using namespace std; 

string configpath(char **); 

int main (int argc, char *argv[])
{
    Fieldplayer statesplayer(argv[4]);
    ofstream teamconfig;

    if(argc<5){
	cerr<<"too few arguments. usage: ./statesplayer x y id teamname field"<<endl; 
	exit(1); 
	}

    int X, Y; 
    string path=configpath(argv);
    statesplayer.setConfigpath(path.c_str()); 
    statesplayer.setID(atoi(argv[3]));
//cout <<statesplayer.getID(); 
	X = atoi(argv[1]);	
	Y = atoi(argv[2]);

 //   if(statesplayer.getID()==1 || statesplayer.getID()==0)
//	teamconfig.open(path.c_str(), ios :: trunc);

     
    teamconfig.open(path.c_str(), ios :: app);

    string player;
    statesplayer.field=argv[5];  
   // cout <<statesplayer.field; 
    player=statesplayer.field+"player";
    teamconfig<<player<<statesplayer.getID()<<" "<<X<<" "<<Y<<endl; 	

    statesplayer.move(X,Y);

    while (1)
    {
        statesplayer.handle_state();
    }

    return 0;
}

string configpath(char *argv[])
{
    string trial= realpath(argv[0], NULL);
    string robocuppath="/Robocup";
    size_t found= trial.find(robocuppath)+robocuppath.size();
    string teamname; 
    teamname=argv[4]; 
    string configpath="/config/"+teamname+"config.txt";
    trial.replace(found, configpath.size(), configpath); 

    return trial;
}

