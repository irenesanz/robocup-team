#include "../../libraries/sockets/udp_socket.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string.h>
using namespace std;

int main(int argc, char *argv[])

{

	// Diferentes comandos para iniciar el programa, con más o menos información.

	char *server ;
	int portno, X, Y, rol ;

	if (argc==6) // ./p1 localhost 6000 ROL X Y 
	{
	  server = argv[1] ;
	  portno = atoi(argv[2]) ;
	  rol = atoi(argv[3]);   //goalie? !=1
	  X = atoi(argv[4]) ;
	  Y = atoi(argv[5]) ;
	}

	else if (argc==5) // ./p1 6000 ROL X Y 
		{
	  		server = "localhost" ;
	  		portno = atoi(argv[1]) ;
	  		rol = atoi(argv[2]);   //goalie? !=1
	  		X = atoi(argv[3]) ;
	  		Y = atoi(argv[4]) ;
		}	

		else if (argc==4) // ./p1 ROL X Y 
			{	
	  			server = "localhost" ;
	  			portno = 6000;
	  			rol = atoi(argv[1]);   //goalie? !=1
	  			X = atoi(argv[2]) ;
	  			Y = atoi(argv[3]) ;
			}
			else if (argc==3) // ./p1 X Y
				{
	  				server = "localhost" ;
	  				portno = 6000 ;
	  				rol = 1;   //Not goalie
	  				X = atoi(argv[1]) ;
	  				Y = atoi(argv[2]) ;
				}
				else 
				{
        				cout <<"use this format: ./p1 [(localhost) [(6000) [(ROL) X Y]]]" << endl;
        				exit(1);
    				}

    UDP_socket clientUDP;

    string message, response;

    clientUDP.connect(server);

    if (clientUDP.getServer()==0) 
	{
	clientUDP.error("Unknown host");
	exit(1);
	}

    clientUDP.setPortno(portno);

	if (rol==1)
	{
        	clientUDP << "(init JINS)\n";
	}
	else
	{
		clientUDP << "(init JINS (goalie))\n";
	}
        clientUDP >> response;

        clientUDP.setReceiverAdress();

	//clientUDP.printAll(); -- Comprobamos que coinciden las IP

        cout << response << endl;

	stringstream aux_init, auxmessage;

	aux_init << "(move " << X << " " << Y << ")" << endl;
	
	clientUDP << aux_init.str();
	//Aquí meter 2 threads para tener procesos paralelos de escritura y lectura/almacenaje
	
	//auxmessage << "(kick 100 0)" << endl;
		//clientUDP << auxmessage.str();

	while(1)
	{

		message.clear();
		response.clear();
	
	
		auxmessage << "(dash 70)" << endl;
		clientUDP << auxmessage.str();

		clientUDP >> response;

        	cout << response << endl;

	}

    return 0;
}


/************************************************************************************

 ---> OPCIONES QUE SE PUEDEN MANDAR POR TERMINAL -- Id recopilando, con sintaxis <---

init --> (init TeamName)
move  ---->  (move X Y) [x e [-54,54], y e [-32,32]] --> (move 10 -10)
reconnect ---> (reconnect TeamName UniformNumber)  --> (reconnect JINS 10)
quit - kill - say bye! ---> (bye)

dash
catch ball (only goalie)
turn (left or right) an angle ----------> (turn direction)
kick the ball ---------->  (kick power direcction) --> (kick 50 -90)


change view
current position
show information
display body state
display match state
display field state (La pista :P)

send again a command (when UDP fails¿?)

*******************************************************************************************/
